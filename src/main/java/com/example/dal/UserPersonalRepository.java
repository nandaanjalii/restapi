package com.example.dal;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.example.model.UserPersonalRec;

@Repository
public interface UserPersonalRepository extends MongoRepository<UserPersonalRec, String> {
}
