package com.example.model;



public class UserOrganizationExpRec {

	private String orgName;
	private String designation;
	private String expFrom;
	private String expTo;
	
	
	public String getOrgName() {
		return orgName;
	}
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public String getExpFrom() {
		return expFrom;
	}
	public void setExpFrom(String expFrom) {
		this.expFrom = expFrom;
	}
	public String getExpTo() {
		return expTo;
	}
	public void setExpTo(String expTo) {
		this.expTo = expTo;
	}
	
	
	
}
