package com.example.model;


import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;



public class UserPersonalRec {

	@Id
	private String emailid;
	private String name;
	private String dob;
	private String gender; 
	private String location;  
	private long    phoneNum;
	private long    whatsappNum;
	private List<UserQualificationsRec> qualifications = new ArrayList<>();
	private List<UserInterestRec>	interests = new ArrayList<>();	
	private List<UserSkillsRec>     skills = new ArrayList<>();
	private List<UserOrganizationExpRec> expDetails = new ArrayList<>();
	
	
	
	public List<UserInterestRec> getInterests() {
		return interests;
	}
	public List<UserOrganizationExpRec> getExpDetails() {
		return expDetails;
	}
	public void setExpDetails(List<UserOrganizationExpRec> expDetails) {
		this.expDetails = expDetails;
	}
	public List<UserSkillsRec> getSkills() {
		return skills;
	}
	public void setSkills(List<UserSkillsRec> skills) {
		this.skills = skills;
	}
	public void setInterests(List<UserInterestRec> interests) {
		this.interests = interests;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public long getPhoneNum() {
		return phoneNum;
	}
	public void setPhoneNum(long phoneNum) {
		this.phoneNum = phoneNum;
	}
	public long getWhatsappNum() {
		return whatsappNum;
	}
	public void setWhatsappNum(long whatsappNum) {
		this.whatsappNum = whatsappNum;
	}
	public String getEmailid() {
		return emailid;
	}
	public void setEmailid(String emailid) {
		this.emailid = emailid;
	}
	public List<UserQualificationsRec> getQualifications() {
		return qualifications;
	}
	public void setQualifications(List<UserQualificationsRec> qualifications) {
		this.qualifications = qualifications;
	}

	
	
}
