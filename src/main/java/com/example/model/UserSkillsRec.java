package com.example.model;


public class UserSkillsRec {

	private String skillName;
	private int    rating;
	
	public String getSkillName() {
		return skillName;
	}
	public void setSkillName(String skillName) {
		this.skillName = skillName;
	}
	public int getRating() {
		return rating;
	}
	public void setRating(int rating) {
		this.rating = rating;
	}
		
	
	
}
