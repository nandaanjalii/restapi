package com.example.controller;

import java.net.URI;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import org.springframework.web.bind.annotation.RestController;

import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.example.dal.UserPersonalRepository;
import com.example.model.UserPersonalRec;


@RestController
@RequestMapping(value = "/")
@EnableAutoConfiguration
public class LinksAppController {
	@Autowired

	private final UserPersonalRepository userRepository;
	

	public LinksAppController(UserPersonalRepository userRepository) {
		this.userRepository = userRepository;
		
	}

	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public List<UserPersonalRec> doHome() {
		return userRepository.findAll();
	}

	@RequestMapping(value = "/home/{sno}", method = RequestMethod.GET)
	public UserPersonalRec doHomeGetOne(@PathVariable String sno) {
		return userRepository.findOne(sno);
	}
	
	@RequestMapping(value = "/update/{sno}", method = RequestMethod.POST)
	public ResponseEntity<Object>doUpdate(@RequestBody UserPersonalRec userrec, @PathVariable String sno) {
		UserPersonalRec userrec1 = userRepository.findOne(sno);

		if (userrec1 == null)
			return ResponseEntity.notFound().build();

		userrec.setEmailid(sno);
		
		userRepository.save(userrec);

		return ResponseEntity.noContent().build();
		
	}
	
	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseEntity<Object> addNewUsers(@RequestBody UserPersonalRec user) {
		UserPersonalRec userrec = userRepository.save(user);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(userrec.getEmailid()).toUri();

		return ResponseEntity.created(location).build();
}

/*	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public String deleteAllUsers() {
		userRepository.deleteAll();
		return "Deleted";
	}
*/

}
